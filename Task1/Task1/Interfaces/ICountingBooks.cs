﻿using System;
using System.Collections;
using Task1.Classe;
using Task1.Classes;

namespace Task1.Interfaces
{
    interface ICountingBooks
    {
        int NumbOfBooksInDepartment(Department department);
        int BooksOfAuthors(Author author);
        int NumbOfBooksInLibrary();
    }
}
