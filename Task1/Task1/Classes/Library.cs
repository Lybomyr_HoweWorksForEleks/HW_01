﻿using System;
using System.Collections;
using Task1.Classe;
using Task1.Classes;
using Task1.Interfaces;

namespace Task1.Classes
{
    class Library : ICountingBooks
    {
        string name;
        public ArrayList departments = new ArrayList();

        public ArrayList Departments
        {
            get
            {
                return departments;
            }

            set
            {
                departments = value;
            }
        }



        public Library(string name)
        {
            this.name = name;
        }

        public int BooksOfAuthors(Author author)
        {
            return author.NumbOfAuthorsBooks;
        }

        public int NumbOfBooksInDepartment(Department department)
        {
            return department.Books.Count;
        }

        public int NumbOfBooksInLibrary()
        {
            int numbOfBooksInLibrary = 0;
            Department departmentInLibrary;
            for (int i = 0; i < this.departments.Count; i++)
            {
                if ((departmentInLibrary = this.departments[i] as Department) != null)
                {
                    numbOfBooksInLibrary += departmentInLibrary.Books.Count;
                }
            }
            return numbOfBooksInLibrary;
        }
    }
}
