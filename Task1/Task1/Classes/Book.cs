﻿using System;
using System.Collections;
using Task1.Classe;

namespace Task1.Classes
{
    class Book : Author, IComparable
    {
        string name;
        public int numbOfPages;

        public int NumbOfPages
        {
            get
            {
                return numbOfPages;
            }

            set
            {
                numbOfPages = value;
            }
        }

        public Book(string name, string author, int numbOfPages)
            : base(author)

        {
            

            books.Add(name);
            this.name = name;
            this.NumbOfPages = numbOfPages;
        }

        public override string ToString()
        {
            return "Name of book - " + this.name + "; Author - "
                + base.authorName + "; Number of pages - " + this.NumbOfPages;
        }

        int IComparable.CompareTo(object obj)
        {
            Book comparingBooks = obj as Book;
            if (comparingBooks != null)
                return this.NumbOfPages.CompareTo(comparingBooks.NumbOfPages);
            else
                throw new Exception("Imposible to compare this books!");
        }
    }
}
