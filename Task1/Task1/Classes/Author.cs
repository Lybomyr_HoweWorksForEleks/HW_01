﻿using System;
using System.Collections;
using Task1.Classe;
using Task1.Classes;

namespace Task1.Classe
{
    class Author : IComparable
    {
        protected string authorName;
        int numbOfAuthorsBooks;
        protected ArrayList books = new ArrayList();

        public Author(string name)
        {
            this.authorName = name;
        }

        public int NumbOfAuthorsBooks
        {
            get
            {
                return numbOfAuthorsBooks;
            }

            set
            {
                numbOfAuthorsBooks = value;
            }
        }

        public int CompareTo(object obj)
        {
            Department comparingDepartments = obj as Department;
            if (comparingDepartments != null)
                return this.NumbOfAuthorsBooks.CompareTo(comparingDepartments.NumbOfBooks);
            else
                throw new Exception("Imposible to compare this authors!");
        }

        public override string ToString()
        {
            return "Author name - " + this.authorName + "; Number of author books - "
                + this.books.Count;
        }
    }
}
