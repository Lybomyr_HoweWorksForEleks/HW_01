﻿using System;
using System.Collections;
using Task1.Classe;
using Task1.Classes;

namespace Task1.Classes
{
    class Department : IComparable
    {
        string name;
        ArrayList books = new ArrayList();
        int numbOfBooks;

        public int NumbOfBooks
        {
            get { return numbOfBooks; }
            set { numbOfBooks = value; }
        }

        public ArrayList Books
        {
            get
            {
                return books;
            }

            set
            {
                books = value;
            }
        }


        public Department(string name)
        {
            this.name = name;
        }

        public int CompareTo(object obj)
        {
            Department comparingDepartments = obj as Department;
            if (comparingDepartments != null)
                return this.books.Count.CompareTo(comparingDepartments.books.Count);
            else
                throw new Exception("Imposible to compare this Departments!");
        }
        public override string ToString()
        {
            return "Name of department - " + this.name + "; Numb of books - " + books.Count;
        }
    }
}
