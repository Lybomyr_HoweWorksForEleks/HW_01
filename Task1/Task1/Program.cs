﻿using System;
using Task1.Classe;
using Task1.Classes;
using Task1.Interfaces;

namespace Task1
{ 
    //test class
    class Program
    {
        static void Main(string[] args)
        {
            
            //creating books
            Book[] books = new Book[9]; 
            books[0] = new Book("Berlin Express", "Mark Ostin", 323);
            books[1] = new Book("Board Games", "Butler James", 843);
            books[2] = new Book("Berlin Express", "Mark Ostin", 323);
            books[3] = new Book("JavaScript Pro3", "Tim Link", 1233);
            books[4] = new Book("JavaScript Pro3", "Tim Link", 1233);
            books[5] = new Book("C# Professional1", "Aleks Shavchuk", 192);
            books[6] = new Book("JavaScript Pro2", "Tim Link", 133);
            books[7] = new Book("C# Professional2", "Aleks Shavchuk", 592);
            books[8] = new Book("JavaScript Pro", "Tim Long", 123);
            //creating departments
            Department department1 = new Department("IT");
            Department department2 = new Department("Fiction");
            Department department3 = new Department("Fantastik");

            //sorting books with IComparable
            Console.WriteLine("Sorting");

            //adding books to department
            department1.Books.Add(books[3]);
            department1.Books.Add(books[4]);
            department1.Books.Add(books[5]);
            department1.Books.Add(books[6]);
            department1.Books.Add(books[7]);
            department1.Books.Add(books[8]);

            department2.Books.Add(books[1]);
            

            //creating library
            Library library1 = new Library("Ivan Franko library");
            library1.Departments.Add(department1);
            library1.Departments.Add(department2);
            library1.Departments.Add(department3);
            //adding new Author
            Author author = new Author("Aleks Shavchuk");
            author.NumbOfAuthorsBooks = 5;

            Author author2 = new Author("Tim Link");
            Console.WriteLine("AUTHOR");
           // Console.WriteLine(author.);
            //author2.NumbOfAuthorsBooks = 4;

            //using interface IComparable
            Array.Sort(books);
            foreach (Book item in books)
            {
                Console.WriteLine(item.numbOfPages);
            }

            
            //using methods which is in IcountingBooks
            ICountingBooks countingBooks = library1; 
            
            Console.WriteLine(countingBooks.NumbOfBooksInLibrary());
            Console.WriteLine(countingBooks.NumbOfBooksInDepartment(library1.departments[0] as Department));
            Console.WriteLine(department1.ToString());
            Console.ReadLine();
        }
    }
}
