﻿using System;
namespace Task3
{
    class Program
    {
        static MyRectangularMatrixTask1 task1 = new MyRectangularMatrixTask1();
        static MatrixWithSaddlePointTask2 task2 = new MatrixWithSaddlePointTask2(); 
        static void Main()
        {
            Console.WriteLine("Choose a task 1 - first, 2 - second task");
            byte key = Convert.ToByte(Console.ReadLine());
            switch (key)
            {
                case 1:
                    {
                        task1.ResultTask1();
                    }
                    break;
                case 2:
                    {
                        task2.ResultForTask2();
                    }
                    break;
                default:
                    break;
            }
            Console.ReadLine();
        }
    }
}
