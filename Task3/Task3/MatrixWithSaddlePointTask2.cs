﻿using System;

namespace Task3
{
    class MatrixWithSaddlePointTask2 : MyRectangularMatrixTask1
    {
        int[] SearchMinOfRows()
        {
            int[] minOfRows = new int[numbOfRows];
            int min = Int32.MaxValue;
            for (int i = 0; i < numbOfRows; i++)
            {
                for (int j = 0; j < numbOfColumns; j++)
                {
                    if (matrix[i, j] < min)
                    {
                        min = matrix[i, j];
                    }
                }

                minOfRows[i] = min;
                min = Int32.MaxValue;
            }

            return minOfRows;
        }

        int[] SearchMaxOfColumns()
        {
            int[] maxOfColumns = new int[numbOfColumns];
            int max = Int32.MinValue;
            for (int j = 0; j < numbOfColumns; j++)
            {
                for (int i = 0; i < numbOfRows; i++)
                {
                    if (matrix[i, j] > max)
                    {
                        max = matrix[i, j];
                    }
                }

                maxOfColumns[j] = max;
                max = Int32.MinValue;
            }


            return maxOfColumns;

        }

        void CalculatingForTask2()
        {
            int[] minOfRows = SearchMinOfRows();
            
            int[] minsortedMass = minOfRows.Clone() as int[];
            Array.Sort(minsortedMass);
           
            for (int i = 0; i < numbOfRows; i++)
            {
                for (int j = 0; j < numbOfColumns; j++)
                {
                    if (matrix[i, j] == minsortedMass[minsortedMass.Length - 1] && FindMax(matrix[i, j] , j) && FindMin(matrix[i,j], i))
                    {
                        Console.WriteLine(matrix[i,j] + " [ " + (i+1) + ", " + (j+1) + "]");

                    }
                }
            }
        }

        bool FindMax(int a, int column)
        {
            bool result = false;

            int[] maxOfColumns = SearchMaxOfColumns();
            int[] maxsortedMass = maxOfColumns.Clone() as int[];
            Array.Sort(maxsortedMass);
            for (int i = 0; i < numbOfRows; i++)
            {
                if (matrix[i, column] == maxsortedMass[0])
                {
                    result = true;
                    break;
                }
                else
                {
                    result = false;
                }
            }

            return result;
        }

        bool FindMin(int a, int row)
        {
            bool result = false;
            int[] minOfRows = SearchMinOfRows();

            int[] minsortedMass = minOfRows.Clone() as int[];
            Array.Sort(minsortedMass);

            for (int j = 0; j < numbOfColumns; j++)
            {
                if (matrix[row, j] == minsortedMass[minsortedMass.Length - 1])
                {
                    result = true;
                    break;
                }
                else
                {
                    result = false;
                }
            }

            return result;
        }






        public void ResultForTask2()
        {

            CreatingMatrixFrFile();
            ShowMatrix();
            CalculatingForTask2();
            /*int[] minOfRows = SearchMinOfRows();
            int[] maxOfColumns = SearchMaxOfColumns();

            int[] minsortedMass = minOfRows.Clone() as int[];
            int[] maxsortedMass = maxOfColumns.Clone() as int[];
            Array.Sort(minsortedMass);
            Array.Sort(maxsortedMass);

            for (int i = 0; i < maxsortedMass.Length; i++)
            {
                Console.WriteLine(maxsortedMass[i]);
            }

            for (int i = 0; i < minsortedMass.Length; i++)
            {
                Console.WriteLine(maxsortedMass[i]);
            }

            Console.WriteLine("sssss");
            if (minsortedMass[minsortedMass.Length - 1] == maxsortedMass[0])
            {
                Console.WriteLine(maxsortedMass[0]);
            }
            */
            
        }
    }
}
