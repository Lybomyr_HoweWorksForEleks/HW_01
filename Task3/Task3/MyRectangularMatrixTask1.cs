﻿using System;
using System.IO;

namespace Task3
{
    class MyRectangularMatrixTask1
    {
        protected int[,] matrix;
        protected int numbOfRows;//i
        protected int numbOfColumns;//j

        protected void CreatingMatrixFrFile()
        {
            string[] read = File.ReadAllLines("testmatrix3.txt");
            numbOfRows = read.Length;
            string[] splitingLine;
            
            for (int i = 0; i < numbOfRows; i++)
            {
                splitingLine = read[i].Split(',');
                if (i == 0)
                {
                    numbOfColumns = splitingLine.Length;
                    matrix = new int[numbOfRows, numbOfColumns];
                }
                for (int j = 0; j < numbOfColumns; j++)
                {
                    matrix[i, j] = Convert.ToInt32(splitingLine[j]);
                }
            }
        }

        protected void ShowMatrix()
        {
            for (int i = 0; i < numbOfRows; i++)
            {
                for (int j = 0; j < numbOfColumns; j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
        }

        void CalculatingForTask1()
        {
            int k;
            int sum = 0;
            for (int i = 0; i < numbOfRows; i++)
            {
                for (int j = 0; j < numbOfColumns; j++)
                {
                    if (matrix[i,j] < 0)
                    {
                        k = i;
                        for (int f = 0; f < numbOfColumns; f++)
                        {
                            sum += matrix[k, f]; 
                        }
                        Console.WriteLine("Numb of row: " + (k + 1) + " sum = " + sum);
                        sum = 0;

                        break;
                    }
                }
            }
        }   

        public void ResultTask1()
        {
            CreatingMatrixFrFile();
            ShowMatrix();
            CalculatingForTask1();
        }
    }
}
