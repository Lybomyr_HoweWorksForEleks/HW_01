﻿using System;

namespace Task4
{
    class Part2 : Part1
    {
        void CalculatingForTask2()
        {
            int k;
            int sum = 0;
            for (int i = 0; i < numbOfRows; i++)
            {
                for (int j = 0; j < numbOfColumns; j++)
                {
                    if (matrix[i, j] < 0)
                    {
                        k = i;
                        for (int f = 0; f < numbOfColumns; f++)
                        {
                            sum += matrix[k, f];
                        }
                        Console.WriteLine("Numb of row: " + (k + 1) + " sum = " + sum);
                        sum = 0;

                        break;
                    }
                }
            }
        }


        public void ResultTask2()
        {
            CreatingMatrixFrFile();
            ShowMatrix();
            CalculatingForTask2();
            
        }
    }
}
