﻿using System;
using System.IO;

namespace Task4
{
    class Part1
    {
        protected int[,] matrix;
        protected int numbOfRows;//i
        protected int numbOfColumns;//j

        protected void CreatingMatrixFrFile()
        {
            string[] read = File.ReadAllLines("testmatrix.txt");
            numbOfRows = read.Length;
            string[] splitingLine;

            for (int i = 0; i < numbOfRows; i++)
            {
                splitingLine = read[i].Split(',');
                if (i == 0)
                {
                    numbOfColumns = splitingLine.Length;
                    if (numbOfRows == numbOfColumns)
                    {
                        matrix = new int[numbOfRows, numbOfColumns];
                    }
                    else
                    {
                        Console.WriteLine("Please enter square matrix");
                        Environment.Exit(0);
                    }
                }
                for (int j = 0; j < numbOfColumns; j++)
                {
                    matrix[i, j] = Convert.ToInt32(splitingLine[j]);
                }
            }
        }
        protected void ShowMatrix()
        {
            for (int i = 0; i < numbOfRows; i++)
            {
                for (int j = 0; j < numbOfColumns; j++)
                {
                    Console.Write(matrix[i, j] + " ");
                }
                Console.WriteLine();
            }
        }
        

        void Calculating()
        {
            int k = 0;
            //numbOfrows = numbOfColumns
            for (int i = 0; i < numbOfRows; i++)
            {
                for (int j = 0; j < numbOfColumns; j++)
                {
                    if (matrix[i,j] == matrix[j,i])
                    {
                        k++;
                    }
                    else
                    {
                        break;
                    }
               
                }

                if (k == numbOfRows)
                {
                    Console.WriteLine( "k = " + (i + 1));
                }
                k = 0;
            }

        }

        public void Result()
        {
            CreatingMatrixFrFile();
            ShowMatrix();
            Calculating();
        }


    }
}
