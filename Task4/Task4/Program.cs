﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Task4
{
    class Program
    {
        static Part1 instOfPart1 = new Part1();
        static Part2 instOfPart2 = new Part2();
        static void Main(string[] args)
        {
            Console.WriteLine("Please check task 1 - first, 2 - second");
            byte key = Convert.ToByte(Console.ReadLine());

            switch (key)
            {
                case 1:
                    {
                        instOfPart1.Result();
                    }
                    break;
                case 2:
                    {
                        instOfPart2.ResultTask2();
                    }
                    break;
                default:
                    break;
            }


            Console.ReadLine();
        }
    }
}
