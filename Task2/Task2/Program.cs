﻿using System;

namespace Task2
{
    class Program
    {
        static int[,] matrix;
        static int numbOfColumn;
        static int numbOfRows;

        static void CreatingMatrix(int[,] matrix, int numbOfRows, int numbOfColumns)
        {
            Console.WriteLine("Please enter elements of matrix");
            for (int i = 0; i < numbOfRows; i++)
            {
                for (int j = 0; j < numbOfColumns; j++)
                {
                    Console.WriteLine("a [" + i + ", " + j + "]");
                    matrix[i, j] = Convert.ToInt32(Console.ReadLine());
                }
            }
        }

        static void CheangingMatrix(int[,] matrix, byte[] mass)
        {
            byte[] sortedMass = new byte[mass.Length];
            int[,] newMatrix = new int[numbOfRows, numbOfColumn]; 
            for (int i = 0; i < mass.Length; i++)
            {
                sortedMass[i] = mass[i];
            }
            Array.Sort(sortedMass);
            Array.Reverse(sortedMass);
           

            Console.WriteLine("result");
            for (int i = 0; i < numbOfRows; i++)
            {
                int k = Array.IndexOf(mass, sortedMass[i]);
                for (int j = 0; j < numbOfColumn; j++)
                {
                    
                    newMatrix[i, j] = matrix[k, j];
                }
                   
            }

            ShowingMatrix(newMatrix, numbOfRows, numbOfColumn);
        }


        static void ShowingMatrix(int[,] matrix, int numbOfRows, int numbOfColum)
        {
            Console.WriteLine("Matrix is:");
            for (int i = 0; i < numbOfRows; i++)
            {
                for (int j = 0; j < numbOfColum; j++)
                {
                    Console.Write(" " + matrix[i,j]);
                }
                Console.WriteLine();
            }
        }

        static int FirstTask()
        {
            int Column = 0;
            int point = 0;
            
             for (int j = 0; j < Program.numbOfColumn; j++)
             {
                for (int i = 0; i < Program.numbOfRows; i++)
                {
                    if (matrix[i,j] < 0)
                    {
                        point = 0;
                        break;
                        
                    }
                    point++;
                }
                if (point == Program.numbOfRows)
                {
                    Column = j;
                    break;
                }
            }
            if (point == 0)
            {
                Console.WriteLine("Every column in matrix have negative value!");
                return 0;
            }
            else
            return Column + 1;
        }
        //i - row, j - column
        static void SecondTask(int[,] matrix)
        {
            int[] mass = new int[numbOfColumn];
            byte[] massForChangeLocation = new byte[numbOfRows];
            for (int i = 0; i < numbOfRows; i++)
            {
                for (int j = 0; j < numbOfColumn; j++)
                {
                    mass[j] = matrix[i, j];
                }
                Array.Sort(mass);
                massForChangeLocation[i] = GetNumbOfRepeatingValues(mass);
            }

            CheangingMatrix(matrix, massForChangeLocation);

        }

        static byte GetNumbOfRepeatingValues(int[] mass)
        {
            byte result = 0;

            for (int i = 0; i < mass.Length; i++)
            {
                if (i == mass.Length-1)
                {
                    if (mass[mass.Length - 2] == mass[mass.Length - 1])
                    {
                        result++;
                    }
                }

                else if (mass[i] == mass[i+1])
                {
                    result++;
                }
            }
            return result;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Please check task(1 - First, 2 - Second)");
            byte key = Convert.ToByte(Console.ReadLine());

            Console.WriteLine("Please enter numb of columns");
            numbOfColumn = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Please enter numb of rows");
            numbOfRows = Convert.ToInt32(Console.ReadLine());
            matrix = new int[numbOfRows, numbOfColumn];

            CreatingMatrix(matrix, numbOfRows, numbOfColumn);
            ShowingMatrix(matrix, numbOfRows, numbOfColumn);
            switch (key)
            {
                case 1:
                    {
                        Console.WriteLine(FirstTask());
                    }
                    break;
                case 2:
                    {
                        SecondTask(matrix);
                    }
                    break;
                default:
                    break;
            }
            Console.ReadLine();
        }
    }
}
